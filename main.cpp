#define GLEW_STATIC
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <math.h>
#include <unistd.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GLFW/glfw3.h>

#define SCREENWIDTH 1000
#define SCREENHEIGHT 1000

GLuint CompileShaderFromFile(char *File, GLenum Type)
{
    printf("Compiling shader ");
    printf(File);
    printf(":\n");

    std::ifstream Input(File);
    std::string SourceString;

    if (!Input.is_open())
        printf("Failed to load shader!\n");

    while (Input.good())
    {
        std::string CurrentLine;
        std::getline(Input, CurrentLine);
        SourceString.append(CurrentLine + "\n");
    }

    GLuint Shader = glCreateShader(Type);
    const char *Source = SourceString.c_str();
    glShaderSource(Shader,1,&Source,NULL);
    glCompileShader(Shader);

    GLint Test;
 	glGetShaderiv(Shader, GL_COMPILE_STATUS, &Test);
 	if(!Test)
    {
        char Buffer[512];
        glGetShaderInfoLog(Shader, 512, NULL, Buffer);
        printf("Failed to compile shader!\n");
        printf(Buffer);
        printf("-\n");
        printf(Source);
        printf("\n-\n");
    }

 	return Shader;
}

GLuint CreateProgram(char *VertShader, char *FragShader)
{
    GLuint VertexShader = CompileShaderFromFile(VertShader, GL_VERTEX_SHADER);
    GLuint FragmentShader = CompileShaderFromFile(FragShader, GL_FRAGMENT_SHADER);

    GLuint ShaderProgram = glCreateProgram();
    glAttachShader(ShaderProgram, VertexShader);
    glAttachShader(ShaderProgram, FragmentShader);
    glDeleteProgram(VertexShader);
    glDeleteProgram(FragmentShader);

    glLinkProgram(ShaderProgram);
    glUseProgram(ShaderProgram);
    return ShaderProgram;
}

float Nodes[2048];
GLuint Arcs[2048];
GLuint MST[2048];
int NumberOfNodes = 0;
int NumberOfArcs = 0;

void PrimsAlgorithm()
{
    float DistanceMatrix[NumberOfNodes][NumberOfNodes];

    for (int m = 0; m < NumberOfNodes; m++)
    {
        for (int n = 0; n < NumberOfNodes; n++)
        {
            DistanceMatrix[m][n] = 0.0;
        }
    }

    printf("Number of nodes: %d\n", NumberOfNodes);
    printf("Number of arcs: %d\n", NumberOfArcs);

    for (int i = 0; i/2 < NumberOfArcs; i += 2)
    {
        printf("Arc %d matches node %d to node %d\n", i/2, Arcs[i], Arcs[i + 1]);

        float x1, y1, x2, y2;
        x1 = Nodes[Arcs[i] * 2];            //h e l p
        y1 = Nodes[Arcs[i] * 2 + 1];        //e e l p
        x2 = Nodes[Arcs[i + 1] * 2];        //l l l p
        y2 = Nodes[Arcs[i + 1] * 2 + 1];    //p p p p

        float dx = x2 - x1;
        float dy = y2 - y1;

        float Distance = sqrt(dx * dx + dy * dy);
        DistanceMatrix[Arcs[i]][Arcs[i + 1]] = Distance;
        DistanceMatrix[Arcs[i + 1]][Arcs[i]] = Distance;

        printf("Node %d: (%f,%f)   Node %d: (%f,%f)   Length of arc: %f\n\n", Arcs[i], x1, y1, Arcs[i + 1], x2, y2, DistanceMatrix[Arcs[i]][Arcs[i] + 1]);
    }

    printf ("* ");
    for (int i = 0; i < NumberOfNodes; i++) printf ("%d        ", i);

    for (int i = 0; i < NumberOfNodes; i++)
    {
        printf("\n%d", i);

        for (int u = 0; u < NumberOfNodes; u++)
        {
            if (DistanceMatrix[u][i] == 0) printf ("    --   ");
            else printf(" %f", DistanceMatrix[u][i]);
        }
    }

    printf("\n\n\n");

    float SmallestConnectedArcLength;
    int Node1, Node2;
    bool ConnectedNodes[NumberOfNodes];
    ConnectedNodes[0] = true;
    for (int i = 1; i < NumberOfNodes; i++) ConnectedNodes[i] = false;

    float MSTLength = 0.0;

    for (int NodesConnected = 0; NodesConnected < NumberOfNodes - 1; NodesConnected++)
    {
        SmallestConnectedArcLength = 999.0;

        for (int i = 0; i < NumberOfNodes; i++)
        {
            if (ConnectedNodes[i] == true)
            {
                for (int u = 0; u < NumberOfNodes; u++)
                {
                    if (DistanceMatrix[i][u] != 0 && ConnectedNodes[u] == false && DistanceMatrix[i][u] < SmallestConnectedArcLength)
                    {
                        SmallestConnectedArcLength = DistanceMatrix[i][u];
                        Node1 = i;
                        Node2 = u;
                    }
                }
            }
        }

        MST[NodesConnected * 2] = Node1;
        MST[NodesConnected * 2 + 1] = Node2;
        ConnectedNodes[Node2] = true;
        MSTLength += SmallestConnectedArcLength;

        printf ("Connected node %d to node %d\n", Node1, Node2);
    }
}


int main()
{
    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    GLFWwindow* Window = glfwCreateWindow(SCREENWIDTH, SCREENHEIGHT, "Test", NULL, NULL);

    glfwMakeContextCurrent(Window);

    glewExperimental = GL_TRUE;
    glewInit();

    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    GLuint VBO;
    glGenBuffers(1, &VBO);

    GLuint GraphEBO, MSTEBO;
    glGenBuffers(1, &GraphEBO);
    glGenBuffers(1, &MSTEBO);

    GLuint ShaderProgram = CreateProgram("VertexShader.vert", "FragmentShader.frag");

    GLint PositionAttribute = glGetAttribLocation(ShaderProgram, "Point");
    glVertexAttribPointer(PositionAttribute, 2, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(PositionAttribute);

    GLint DrawColour = glGetUniformLocation(ShaderProgram, "RGB");
    glPointSize(10.0);

    bool NodesDone = false;
    bool ArcsDone = false;
    int SelectedNode = 999;
    double PressTime = 0;

    while (!glfwWindowShouldClose(Window))
    {
        glClear(GL_COLOR_BUFFER_BIT);
        glBindVertexArray(VAO);

        if (NodesDone)
        {
            glUniform3f(DrawColour, 1.0, 0.0, 0.0);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GraphEBO);
            glDrawElements(GL_LINES, NumberOfArcs * 2, GL_UNSIGNED_INT, 0);

            if (ArcsDone)
            {
                glUniform3f(DrawColour, 0.0, 1.0, 0.0);
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, MSTEBO);
                glDrawElements(GL_LINES, (NumberOfNodes - 1) * 2, GL_UNSIGNED_INT, 0);
            }
        }

        glUniform3f(DrawColour, 1.0, 1.0, 1.0);
        glDrawArrays(GL_POINTS, 0, NumberOfNodes);

        if (glfwGetKey(Window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
                glfwSetWindowShouldClose(Window, GL_TRUE);

        PressTime = glfwGetTime();
        if (PressTime > 0.5)
        {
            glfwPollEvents();

            if (glfwGetKey(Window, GLFW_KEY_ENTER) == GLFW_PRESS)
            {
                glfwSetTime(0.0);
                if (!NodesDone)
                {
                    NodesDone = true;

                    for (int i = 0; i < NumberOfNodes * 2; i += 2)
                        printf("Node %d: (%f,%f)\n", i/2, Nodes[i], Nodes[i + 1]);
                }

                else
                {
                    ArcsDone = true;
                    PrimsAlgorithm();
                    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, MSTEBO);
                    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(MST), MST, GL_STATIC_DRAW);
                }
            }

            else if (glfwGetMouseButton(Window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS)
            {
                glfwSetTime(0.0);
                double MouseX, MouseY;
                glfwGetCursorPos(Window, &MouseX, &MouseY);

                float NormalisedX = MouseX / SCREENWIDTH * 2 - 1;
                float NormalisedY = (MouseY / SCREENHEIGHT * 2 - 1) * -1;

                printf("MouseX: %f MouseY: %f NormalisedX: %f NormalisedY: %f\n", MouseX, MouseY, NormalisedX, NormalisedY);

                if (!NodesDone)
                {
                    Nodes[NumberOfNodes * 2] = NormalisedX;
                    Nodes[NumberOfNodes * 2 + 1] = NormalisedY;
                    NumberOfNodes++;

                    glBindBuffer(GL_ARRAY_BUFFER, VBO);
                    glBufferData(GL_ARRAY_BUFFER, sizeof(Nodes), Nodes, GL_DYNAMIC_DRAW);

                    GLint PositionAttribute = glGetAttribLocation(ShaderProgram, "Point");
                    glVertexAttribPointer(PositionAttribute, 2, GL_FLOAT, GL_FALSE, 0, 0);
                    glEnableVertexAttribArray(PositionAttribute);
                }

                else if (!ArcsDone)
                {
                    float ShortestDistanceSq = 999.0;
                    int ClosestNode;

                    for (int i = 0; i < NumberOfNodes * 2; i += 2)
                    {
                        float DistanceSq = (NormalisedX - Nodes[i]) * (NormalisedX - Nodes[i]) + (NormalisedY - Nodes[i + 1]) * (NormalisedY - Nodes[i + 1]);
                        if (DistanceSq < ShortestDistanceSq)
                        {
                            ShortestDistanceSq = DistanceSq;
                            ClosestNode = i / 2;
                        }
                    }

                    if (SelectedNode == 999)
                    {
                        SelectedNode = ClosestNode;
                        printf("Selected node %d\n", SelectedNode);
                    }

                    else
                    {
                        Arcs[NumberOfArcs * 2] = SelectedNode;
                        Arcs[NumberOfArcs * 2 + 1] = ClosestNode;
                        NumberOfArcs++;
                        SelectedNode = 999;
                        printf("Connected to node %d\n", ClosestNode);
                        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, GraphEBO);
                        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Arcs), Arcs, GL_DYNAMIC_DRAW);
                    }
                }
            }
        }

        glfwSwapBuffers(Window);

        GLenum ErrorEnum = glGetError();
        if (ErrorEnum != GL_NO_ERROR) printf("glGetError returned '%s'!\n", gluErrorString(ErrorEnum));
    }

    glfwTerminate();
    return 0;
}
