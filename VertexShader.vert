#version 150

in vec2 Point;

void main()
{
    gl_Position = vec4(Point, 0.0, 1.0);
}
