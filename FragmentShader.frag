#version 150

uniform vec3 RGB;
out vec4 Colour;

void main()
{
    Colour = vec4(RGB, 1.0);
}
